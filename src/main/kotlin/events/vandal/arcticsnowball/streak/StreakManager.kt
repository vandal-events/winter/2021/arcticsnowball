package events.vandal.arcticsnowball.streak

import events.vandal.arcticsnowball.ArcticSnowball
import net.kyori.adventure.text.Component
import net.kyori.adventure.text.format.TextColor
import net.kyori.adventure.text.format.TextDecoration
import net.kyori.adventure.text.serializer.legacy.LegacyComponentSerializer
import org.bukkit.ChatColor
import org.bukkit.entity.Player

object StreakManager {
    val streaks = mutableMapOf<Player, Int>()
    private val serializer = LegacyComponentSerializer.legacySection()

    fun addToStreak(player: Player) {
        if (!streaks.contains(player))
            streaks[player] = 0

        streaks[player]!!.inc()

        if (streaks[player]!! % 25 == 0) {
            ArcticSnowball.plugin.server.broadcast(serializer.deserialize("${ChatColor.AQUA}${ChatColor.BOLD}ArcticSnowball >> ${ChatColor.RESET}${ChatColor.DARK_AQUA}${player.name}${ChatColor.WHITE} got a streak of ${ChatColor.GREEN}${streaks[player]}${ChatColor.WHITE}!"))
        }

        player.sendActionBar(serializer.deserialize("${ChatColor.GOLD}${ChatColor.BOLD}Streak : ${ChatColor.AQUA}${streaks[player]}"))
    }

    fun resetStreak(player: Player) {
        streaks[player] = 0

        player.sendActionBar(serializer.deserialize("${ChatColor.GOLD}${ChatColor.BOLD}Streak : ${ChatColor.AQUA}${streaks[player]}"))
    }
}