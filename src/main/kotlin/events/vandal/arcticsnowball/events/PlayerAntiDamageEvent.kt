package events.vandal.arcticsnowball.events

import org.bukkit.entity.Player
import org.bukkit.event.EventHandler
import org.bukkit.event.Listener
import org.bukkit.event.entity.EntityDamageEvent

object PlayerAntiDamageEvent : Listener {
    @EventHandler
    fun onPlayerDamage(ev: EntityDamageEvent) {
        if (ev.entity is Player) {
            if (ev.cause == EntityDamageEvent.DamageCause.VOID || ev.cause == EntityDamageEvent.DamageCause.SUICIDE)
                return
            ev.damage = 0.0000000001
        } else {
            ev.isCancelled = true
        }
    }
}