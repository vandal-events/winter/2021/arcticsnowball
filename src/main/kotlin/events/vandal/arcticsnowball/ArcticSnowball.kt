package events.vandal.arcticsnowball

import events.vandal.arcticsnowball.commands.DisableSnowballsCommand
import events.vandal.arcticsnowball.commands.RMSCommand
import events.vandal.arcticsnowball.events.PlayerAntiDamageEvent
import events.vandal.arcticsnowball.events.SnowballEvent
import org.bukkit.plugin.java.JavaPlugin

class ArcticSnowball : JavaPlugin() {
    override fun onEnable() {
        plugin = this

        if (!this.server.pluginManager.isPluginEnabled("LuckPerms"))
            throw Error("LuckPerms is not installed!")

        this.saveDefaultConfig()
        this.server.pluginManager.registerEvents(SnowballEvent, this)
        this.server.pluginManager.registerEvents(PlayerAntiDamageEvent, this)

        this.getCommand("remove-snowball")?.setExecutor(RMSCommand)
        this.getCommand("remove-snowball")?.tabCompleter = RMSCommand

        this.getCommand("disable-snowballs")?.setExecutor(DisableSnowballsCommand)
    }

    override fun onDisable() {
        // Plugin shutdown logic
    }

    companion object {
        lateinit var plugin: JavaPlugin

        const val SNOWBALL_COOLDOWN = 15
        var snowballsEnabled = true
    }
}
