package events.vandal.arcticsnowball.commands

import org.bukkit.ChatColor
import org.bukkit.Material
import org.bukkit.command.Command
import org.bukkit.command.CommandExecutor
import org.bukkit.command.CommandSender
import org.bukkit.command.TabCompleter
import org.bukkit.inventory.ItemStack
import events.vandal.arcticsnowball.ArcticSnowball
import events.vandal.arcticsnowball.streak.StreakManager
import net.luckperms.api.LuckPerms
import net.luckperms.api.node.Node
import org.bukkit.Bukkit
import org.bukkit.entity.Player

object RMSCommand : CommandExecutor, TabCompleter {
    override fun onCommand(sender: CommandSender, command: Command, label: String, args: Array<out String>): Boolean {
        val luckPerms = Bukkit.getServicesManager().getRegistration(LuckPerms::class.java)!!.provider

        if (sender.hasPermission("arcticsnowball.exempt-op") && !sender.hasPermission("arcticsnowball.rms")) {
            sender.sendMessage("${ChatColor.RED}>> Your ability to use snowballs has currently been revoked by an admin.")

            return true
        }

        if (args.isEmpty() && sender is Player) {
            val user = luckPerms.getPlayerAdapter(Player::class.java).getUser(sender)
            if (sender.hasPermission("arcticsnowball.exempt")) {
                user.data().add(Node.builder("arcticsnowball.exempt").value(false).build())

                if (sender.hasPermission("arcticsnowball.rms"))
                    user.data().add(Node.builder("arcticsnowball.exempt-op").value(false).build())

                if (!sender.inventory.contains(Material.SNOWBALL))
                    sender.inventory.addItem(ItemStack(Material.SNOWBALL, 1))

                sender.sendMessage("${ChatColor.GREEN}>> You are no longer exempt from snowballs.")
            } else {
                user.data().add(Node.builder("arcticsnowball.exempt").build())
                sender.inventory.removeItemAnySlot(ItemStack(Material.SNOWBALL))

                sender.sendMessage("${ChatColor.YELLOW}>> You are now exempt from snowballs.")
            }

            StreakManager.resetStreak(sender)
            luckPerms.userManager.saveUser(user)

            return true
        }

        if (!sender.hasPermission("arcticsnowball.rms")) {
            sender.sendMessage("${ChatColor.RED}>> Incorrect usage! Usage is: ${ChatColor.YELLOW}/toggle-snowball")

            return true
        }

        val player = ArcticSnowball.plugin.server.getPlayer(args[0])

        if (player == null) {
            sender.sendMessage("${ChatColor.RED}Player does not exist or is not online!")
            return true
        }

        val user = luckPerms.getPlayerAdapter(Player::class.java).getUser(player)

        if (player.hasPermission("arcticsnowball.exempt-op")) {
            user.data().add(Node.builder("arcticsnowball.exempt-op").value(false).build())

            if (!player.inventory.contains(Material.SNOWBALL))
                player.inventory.addItem(ItemStack(Material.SNOWBALL, 1))

            sender.sendMessage("${ChatColor.GREEN}>> Allowed ${player.name} to utilize snowballs again.")
            player.sendMessage("${ChatColor.GREEN}>>> An admin has allowed you to utilize snowballs again!")
        } else {
            user.data().add(Node.builder("arcticsnowball.exempt-op").build())
            player.inventory.removeItemAnySlot(ItemStack(Material.SNOWBALL))

            sender.sendMessage("${ChatColor.YELLOW}>> Prevented ${player.name} from utilizing snowballs.")
            player.sendMessage("${ChatColor.RED}>>> An admin has prevented you from utilizing snowballs!")
        }

        StreakManager.resetStreak(player)

        luckPerms.userManager.saveUser(user)

        return true
    }

    override fun onTabComplete(
        sender: CommandSender,
        command: Command,
        alias: String,
        args: Array<out String>
    ): MutableList<String> {
        return if (sender.hasPermission("arcticsnowball.rms")) {
            ArcticSnowball.plugin.server.onlinePlayers.map { it.name }.toMutableList()
        } else {
            mutableListOf()
        }
    }
}