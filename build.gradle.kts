plugins {
    java
    kotlin("jvm") version "1.7.21"
    id("com.github.johnrengelman.shadow") version "7.1.0"
    id("xyz.jpenilla.run-paper") version "1.0.6"
}

group = "events.vandal"
version = "1.1.0"

repositories {
    mavenCentral()
    maven("https://papermc.io/repo/repository/maven-public/")
    maven("https://oss.sonatype.org/content/groups/public/")
}

val minecraftGameVersion: String by project
val paperVersion: String by project
val luckpermsVersion: String by project
dependencies {
    implementation("org.jetbrains.kotlin:kotlin-stdlib")

    compileOnly("io.papermc.paper:paper-api:$minecraftGameVersion-$paperVersion")
    compileOnly("net.luckperms:api:$luckpermsVersion")
}

tasks {
    runServer {
        minecraftVersion(minecraftGameVersion)
    }

    processResources {
        val props = mapOf("version" to project.version)
        inputs.properties(props)
        filteringCharset = "UTF-8"
        filesMatching("plugin.yml") {
            expand(props)
        }
    }

    shadowJar {
        mergeServiceFiles()
    }
}